// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore/lite';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyB3Mh7f-fsqhIfIl8_Zq3tyDjrF2aVrIgY",
  authDomain: "mkr2-d8852.firebaseapp.com",
  projectId: "mkr2-d8852",
  storageBucket: "mkr2-d8852.appspot.com",
  messagingSenderId: "930796829708",
  appId: "1:930796829708:web:30bf19b27d8431e25c4229"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const firebaseDB = getFirestore(app);
export default app;