import { createStore } from 'vuex'
import getModuleSettingsObject from './helpers/GetModuleSettingsObject'
export default createStore({
  ...getModuleSettingsObject('companies')
})
